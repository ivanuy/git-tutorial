

<!DOCTYPE html>
<head>
	<title>Git Tutorials!</title>
</head>
sadasasdasdas
<body style="padding:100px 200px">

	<img name="git-logo" src="" title="Git logo" height="120" width="200" style="display:block" />
	<h1>Welcome to Git Tutorial!</h1>

	<h2>Hello,</h2>
	<h2 name="your-name" style="margin-left:70px">World!</h2>

	</br>
	<h3>Exercise:</h3>
	<ol>
		<li>Open tutorial.html file in your preferred editor/IDE.</li>
		<li>(Dev 1) In tutorial.html, copy and paste the source below in the src value of img tag named "git-logo". (add --> commit --> push)</li>
		<p><i>logo/gitlogo.png</i></p>
		(Dev 2) In tutorial.html, copy and paste the source below in the src value of img tag named "gitlab-logo". (add --> commit --> push)</li>
		<p><i>logo/gitlab-logo.png</i></p>
		<i>(The last person to push shall encounter fail message. You need to update your working copy and do push again. </br>And whoever pushes first should update his/her working copy. At this time, both of you should already have the latest working copy displaying the logo of Git and GitLab.)</i>
		<li>Change the text "World!" in h2 tag named "your-name" with your full name. (add --> commit --> push)</li>
		<i>(The last one who push changes will encounter fail message. Fix the conflicts and do above steps again. Other dev should fetch changes from remote repo in order to update his/her working copy. At this time both of you should already have the same working copy displaying your names.)</i>
		<li>Freestyle time. Make some experiments.</li>
	</ol>

	<div style="margin-top:150px">
		<a>Git Tutorial. Powered by GitLab.com.</a>
		<img name="gitlab-logo" src="" title="Gitlab logo" height="50" width="300" style="display:block" />
	</div>
</body>
</html>
